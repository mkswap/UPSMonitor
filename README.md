# UPSonitor

UPSMonitor is a simple GUI written in Python and GTK that allows you to control NUT servers and monitoring your UPSs. 
This application provides a backand dbus server that constantly check the UPS status changes and a frontend that allows 
you to display the configurations of NUT and UPSs and manage it.

![](https://gitlab.com/mkswap/UPSMonitor/-/raw/main/screenshot/Screenshot%20From%202024-09-18%2014-33-45.png?ref_type=heads)
