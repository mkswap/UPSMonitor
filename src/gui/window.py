# window.py
#
# Copyright 2023 Giorgio Dramis
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import threading, dbus, time, logging

from gi.repository import Adw, Gtk, GObject

from .data_service import NUTClientError
from .ups_monitor_daemon import UPSMonitorClient
from .custom_components import UpsActionRow
from .ups_pages import UpsInfoPage, UpsSettingsPage, UpsNotificationsPage, UpsPreferencesPage, HomePage, OutletsPage
from .add_new_server_box import AddNewServerBox

APPLICATION_ID = 'org.ponderorg.UPSMonitor'
LOG_LEVEL = logging.INFO

@Gtk.Template(resource_path='/org/ponderorg/UPSMonitor/ui/window.ui')
class UpsmonitorWindow(Adw.ApplicationWindow):
    __gtype_name__ = 'UpsmonitorWindow'

    ups_list_box = Gtk.Template.Child()
    add_server_button = Gtk.Template.Child()
    split_view = Gtk.Template.Child()
    navigation_view = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.connect("destroy", self.on_destroy)
        self.navigation_view.push(HomePage())
        self.initialize_log()
        thread = threading.Thread(target=self.start_dbus_connection, daemon = True)
        thread.start()

    def initialize_log(self):
        self._logger = logging.getLogger('UpsmonitorWindow')
        c_handler = logging.FileHandler('.var/app/'+ APPLICATION_ID +'/data/SystemOut_gui.log')
        c_handler.setFormatter(logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
        self._logger.setLevel(LOG_LEVEL)
        self._logger.addHandler(c_handler)

    def start_dbus_connection(self):
        dbus_counter = 0
        while dbus_counter < 10:
            try:
                self._dbus_client = UPSMonitorClient()
                self._dbus_signal_handler = self._dbus_client.connect_to_signal("ups_changed", self.refresh_ups_data)
                self._logger.info("comunication daemon ready")
                self.refresh_ups_data()
                break
            except dbus.exceptions.DBusException as e:
                dbus_counter += 1
                if dbus_counter == 10:
                    self.logger.exception('deamon not ready, max retry reached!', e)
                time.sleep(1)

    def refresh_ups_data(self):
        self.ups_list = []
        self.ups_list.extend(self._dbus_client.get_all_ups())
        while self.ups_list_box.get_last_child() != None:
            child = self.ups_list_box.get_last_child()
            self.ups_list_box.remove(child)
            child.run_dispose()
        for ups in self.ups_list:
            ups_action_row = UpsActionRow(ups_data = ups)
            ups_action_row.connect('activated', self.on_ups_row_selected)
            self.ups_list_box.insert(ups_action_row, -1)

    def on_ups_row_selected(self, widget):
        self.split_view.set_show_content(True)
        ups_preferences_page = UpsPreferencesPage(ups_data=widget.ups_data)
        ups_preferences_page.informations_row.connect('activated', self.on_informations_selected, widget.ups_data)
        ups_preferences_page.settings_row.connect('activated', self.on_settings_selected, widget.ups_data)
        if ups_preferences_page.outlets_row is not None:
            ups_preferences_page.outlets_row.connect('activated', self.on_outlets_selected, widget.ups_data)
        ups_preferences_page.notifications_row.connect('activated', self.on_notifications_selected, widget.ups_data)
        self.navigation_view.replace([ups_preferences_page])
        self.navigation_view.pop_to_tag('ups_page')

    def on_informations_selected(self, widget, ups_data):
        self.navigation_view.push(UpsInfoPage(ups_data=ups_data))

    def on_settings_selected(self, widget, ups_data):
        self.navigation_view.push(UpsSettingsPage(ups_data=ups_data))

    def on_outlets_selected(self, widget, ups_data):
        self.navigation_view.push(OutletsPage(ups_data=ups_data))

    def on_notifications_selected(self, widget, ups_data):
        self.navigation_view.push(UpsNotificationsPage(ups_data=ups_data))

    def on_destroy(self, widget):
        self._dbus_signal_handler.remove()
