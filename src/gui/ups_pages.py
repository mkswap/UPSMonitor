from gi.repository import Adw, Gtk

from .data_model import UPS
from .custom_components import BatteryRow, LoadRow, BeeperRow, CustomRow, CustomExpanderRow, SettingSwitchRow, OutletsRow
from .ups_monitor_daemon import UPSMonitorClient, NotificationType

@Gtk.Template(resource_path='/org/ponderorg/UPSMonitor/ui/ups_preferences_page.ui')
class UpsPreferencesPage(Adw.NavigationPage):
    __gtype_name__ = 'UpsPreferencesPage'

    first_group = Gtk.Template.Child()
    window_title = Gtk.Template.Child()
    informations_row = Gtk.Template.Child()
    settings_row = Gtk.Template.Child()
    notifications_row = Gtk.Template.Child()
    outlets_row = None

    def __init__(self, **kwargs):
        self.ups_data = kwargs.get("ups_data", None)
        if self.ups_data != None:
            kwargs.pop("ups_data")
        super().__init__(**kwargs)
        self._dbus_client = UPSMonitorClient()
        if self.ups_data.host_id == None:
            self.settings_row.set_visible(False)
            self.notifications_row.set_visible(False)
        self.update_self()

    def update_self(self):
        if self.ups_data != None and self.ups_data.host_id != None:
            ups_data = self._dbus_client.get_ups_by_name_and_host(self.ups_data.host_id, self.ups_data.key)
            if ups_data != None:
                self.ups_data = ups_data
        battery_row = BatteryRow(ups_data = self.ups_data)
        self.first_group.add(battery_row)
        load_row = LoadRow(ups_data = self.ups_data)
        self.first_group.add(load_row)
        if self.ups_data._output is not None:
            text = "-"
            if self.ups_data._output.voltage is not None:
                text = str(self.ups_data._output.voltage) + " W"
            row = CustomExpanderRow(title="Output", label=text, icon_name="camera-flash-symbolic")
            if self.ups_data._output.voltage_nominal is not None:
                text = str(self.ups_data._output.voltage_nominal) + " W"
                sub_row = CustomRow(title="Voltage nominal", label=text)
                row.add_row(sub_row)
            if self.ups_data._output.frequency_nominal is not None:
                text = str(self.ups_data._output.frequency_nominal) + " Hz"
                sub_row = CustomRow(title="Frequency nominal", label=text)
                row.add_row(sub_row)
            self.first_group.add(row)
        if self.ups_data._input is not None:
            text = "-"
            if self.ups_data._input.voltage is not None:
                text = str(self.ups_data._input.voltage) + " W"
            elif self.ups_data._input.transfer_high is not None and self.ups_data._input.transfer_low is not None:
                high = int(self.ups_data._input.transfer_high)
                low = int(self.ups_data._input.transfer_low)
                text = str(high) + " W - " + str(low) + " W"
            row = CustomExpanderRow(title="Input", label=text, icon_name="camera-flash-symbolic")
            if self.ups_data._input.transfer_low is not None:
                text = str(self.ups_data._input.transfer_high) + " W"
                sub_row = CustomRow(title="Input high", label=text)
                row.add_row(sub_row)
            if self.ups_data._input.transfer_low is not None:
                text = str(self.ups_data._input.transfer_low) + " W"
                sub_row = CustomRow(title="Input low", label=text)
                row.add_row(sub_row)
            self.first_group.add(row)
        if self.ups_data.outlet_collection.count > 0:
            self.outlets_row = OutletsRow(title="Outlets", label=str(self.ups_data.outlet_collection.count), icon_name="power-symbolic")
            self.first_group.add(self.outlets_row)
        if self.ups_data.beeper_status is not None:
            beeper_row = BeeperRow(ups_data = self.ups_data, icon_name="sound-wave-symbolic", title="Beeper status")
            self.first_group.add(beeper_row)
        if self.ups_data.delay_start is not None:
            text = str(self.ups_data.delay_shutdown) + " ms"
            row = CustomRow(title="Start delay", label=text, icon_name="timer-symbolic")
            self.first_group.add(row)
        if self.ups_data.delay_shutdown is not None:
            text = str(self.ups_data.delay_shutdown) + " ms"
            row = CustomRow(title="Shutdown delay", label=text, icon_name="timer-symbolic")
            self.first_group.add(row)
        if self.ups_data.frequency is not None:
            text = str(self.ups_data.frequency) + " Hz"
            row = CustomRow(title="Status", lable=text, icon_name="charge-symbolic")
            self.first_group.add(row)
        if self.ups_data.status == "OB":
            row = CustomRow(title="Status", label="Offline", icon_name="error-symbolic")
            self.first_group.add(row)
        elif self.ups_data.status == "OL":
            row = CustomRow(title="Status", label="Online", icon_name="check-round-outline-whole-symbolic")
            self.first_group.add(row)
        self.window_title.set_title(self.ups_data.model)

    def on_destroy(self, widget):
        self._dbus_signal_handler.remove()

@Gtk.Template(resource_path='/org/ponderorg/UPSMonitor/ui/ups_informations_page.ui')
class UpsInfoPage(Adw.NavigationPage):
    __gtype_name__ = 'UpsInfoPage'

    window_title = Gtk.Template.Child()
    ups_group = Gtk.Template.Child()
    device_group = Gtk.Template.Child()
    driver_group = Gtk.Template.Child()

    def __init__(self, **kwargs):
        self.ups_data = kwargs.get("ups_data", None)
        if self.ups_data != None:
            kwargs.pop("ups_data")
        super().__init__(**kwargs)
        self._dbus_client = UPSMonitorClient()
        self.window_title.set_title(self.ups_data.model)
        self.update_self()

    def update_self(self):
        if self.ups_data != None and self.ups_data.host_id != None:
            ups_data = self._dbus_client.get_ups_by_name_and_host(self.ups_data.host_id, self.ups_data.key)
            if ups_data != None:
                self.ups_data = ups_data

                # UPS group
                if self.ups_data.firmware is not None:
                    self.ups_group.add(CustomRow(title="Firmware", label=self.ups_data.firmware))
                if self.ups_data.mfr is not None:
                    self.ups_group.add(CustomRow(title="Manufacturer", label=self.ups_data.mfr))
                if self.ups_data.model is not None:
                    self.ups_group.add(CustomRow(title="Model", label=self.ups_data.model))
                if self.ups_data.serial is not None:
                    self.ups_group.add(CustomRow(title="Serial", label=self.ups_data.serial))
                if self.ups_data._type is not None:
                    self.ups_group.add(CustomRow(title="Type", label=self.ups_data._type))
                if self.ups_data.productid is not None:
                    self.ups_group.add(CustomRow(title="Product ID", label=self.ups_data.productid))
                if self.ups_data.vendorid is not None:
                    self.ups_group.add(CustomRow(title="Vendor ID", label=self.ups_data.vendorid))

                # Device group
                if self.ups_data.device.model is not None:
                    self.device_group.add(CustomRow(title="Model", label=self.ups_data.device.model))
                if self.ups_data.device.mfr is not None:
                    self.device_group.add(CustomRow(title="Manufacturer", label=self.ups_data.device.mfr))
                if self.ups_data.device.serial is not None:
                    self.device_group.add(CustomRow(title="Serial", label=self.ups_data.device.serial))
                if self.ups_data.device._type is not None:
                    self.device_group.add(CustomRow(title="Type", label=self.ups_data.device._type))
                if self.ups_data.device.description is not None:
                    self.device_group.add(CustomRow(title="Description", label=self.ups_data.device.description))
                if self.ups_data.device.contact is not None:
                    self.device_group.add(CustomRow(title="Contact", label=self.ups_data.device.contact))
                if self.ups_data.device.location is not None:
                    self.device_group.add(CustomRow(title="Location", label=self.ups_data.device.location))
                if self.ups_data.device.part is not None:
                    self.device_group.add(CustomRow(title="Part", label=self.ups_data.device.part))
                if self.ups_data.device.macaddr is not None:
                    self.device_group.add(CustomRow(title="MAC Address", label=self.ups_data.device.macaddr))
                if self.ups_data.device.uptime is not None:
                    self.device_group.add(CustomRow(title="Uptime", label=self.ups_data.device.uptime))
                if self.ups_data.device.count is not None:
                    self.device_group.add(CustomRow(title="Count", label=self.ups_data.device.count))

                # Driver group
                if self.ups_data.driver.name is not None:
                    self.driver_group.add(CustomRow(title="Driver Name", label=self.ups_data.driver.name))
                if self.ups_data.driver.version is not None:
                    self.driver_group.add(CustomRow(title="Driver Version", label=self.ups_data.driver.version))
                if self.ups_data.driver.version_internal is not None:
                    self.driver_group.add(CustomRow(title="Internal Version", label=self.ups_data.driver.version_internal))
                if self.ups_data.driver.version_data is not None:
                    self.driver_group.add(CustomRow(title="Version Data", label=self.ups_data.driver.version_data))
                if self.ups_data.driver.version_usb is not None:
                    self.driver_group.add(CustomRow(title="USB Version", label=self.ups_data.driver.version_usb))
                if self.ups_data.driver.parameter_xxx is not None:
                    self.driver_group.add(CustomRow(title="Parameter XXX", label=self.ups_data.driver.parameter_xxx))
                if self.ups_data.driver.flag_xxx is not None:
                    self.driver_group.add(CustomRow(title="Flag XXX", label=self.ups_data.driver.flag_xxx))
                if self.ups_data.driver.state is not None:
                    self.driver_group.add(CustomRow(title="State", label=self.ups_data.driver.state))
                if self.ups_data.driver.flag_allow_killpower is not None:
                    self.driver_group.add(CustomRow(title="Flagallow killpower", label=self.ups_data.driver.flag_allow_killpower))
                if self.ups_data.driver.debug is not None:
                    self.driver_group.add(CustomRow(title="Debug", label=self.ups_data.driver.debug))
                if self.ups_data.driver.bus is not None:
                    self.driver_group.add(CustomRow(title="BUS", label=self.ups_data.driver.bus))
                if self.ups_data.driver.pollfreq is not None:
                    self.driver_group.add(CustomRow(title="Polling frequency", label=self.ups_data.driver.pollfreq))
                if self.ups_data.driver.pollinterval is not None:
                    self.driver_group.add(CustomRow(title="Polling interval", label=self.ups_data.driver.pollinterval))
                if self.ups_data.driver.port is not None:
                    self.driver_group.add(CustomRow(title="Port", label=self.ups_data.driver.port))
                if self.ups_data.driver.product is not None:
                    self.driver_group.add(CustomRow(title="Product", label=self.ups_data.driver.product))
                if self.ups_data.driver.productid is not None:
                    self.driver_group.add(CustomRow(title="Product ID", label=self.ups_data.driver.productid))
                if self.ups_data.driver.serial is not None:
                    self.driver_group.add(CustomRow(title="Serial", label=self.ups_data.driver.serial))
                if self.ups_data.driver.synchronous is not None:
                    self.driver_group.add(CustomRow(title="Synchronous", label=self.ups_data.driver.synchronous))
                if self.ups_data.driver.vendor is not None:
                    self.driver_group.add(CustomRow(title="Vendor", label=self.ups_data.driver.vendor))
                if self.ups_data.driver.vendorid is not None:
                    self.driver_group.add(CustomRow(title="Vendor ID", label=self.ups_data.driver.vendorid))

@Gtk.Template(resource_path='/org/ponderorg/UPSMonitor/ui/ups_notifications_page.ui')
class UpsNotificationsPage(Adw.NavigationPage):
    __gtype_name__ = 'UpsNotificationsPage'

    window_title = Gtk.Template.Child()
    offline_notify_switch = Gtk.Template.Child()
    low_battery_notify_switch = Gtk.Template.Child()

    def __init__(self, **kwargs):
        self.ups_data = kwargs.get("ups_data", None)
        if self.ups_data != None:
            kwargs.pop("ups_data")
        super().__init__(**kwargs)
        self._dbus_client = UPSMonitorClient()
        self.window_title.set_title(self.ups_data.model)
        self.notifications = self._dbus_client.get_all_ups_notifications(self.ups_data)
        if NotificationType.IS_OFFLINE in self.notifications:
            self.offline_notify_switch.set_active(True)
        else:
            self.offline_notify_switch.set_active(False)
        if NotificationType.LOW_BATTERY in self.notifications:
            self.low_battery_notify_switch.set_active(True)
        else:
            self.low_battery_notify_switch.set_active(False)

    @Gtk.Template.Callback()
    def offline_notify_switch_selected(self, widget, args):
        if self.offline_notify_switch.get_active() and NotificationType.IS_OFFLINE not in self.notifications:
            self._dbus_client.set_ups_notification_type(self.ups_data, NotificationType.IS_OFFLINE, True)
            self.notifications = self._dbus_client.get_all_ups_notifications(self.ups_data)
        elif not self.offline_notify_switch.get_active() and NotificationType.IS_OFFLINE in self.notifications:
            self._dbus_client.set_ups_notification_type(self.ups_data, NotificationType.IS_OFFLINE, False)
            self.notifications = self._dbus_client.get_all_ups_notifications(self.ups_data)

    @Gtk.Template.Callback()
    def low_battery_notify_switch_selected(self, widget, args):
        if self.low_battery_notify_switch.get_active() and NotificationType.LOW_BATTERY not in self.notifications:
            self._dbus_client.set_ups_notification_type(self.ups_data, NotificationType.LOW_BATTERY, True)
            self.notifications = self._dbus_client.get_all_ups_notifications(self.ups_data)
        elif not self.low_battery_notify_switch.get_active() and NotificationType.LOW_BATTERY in self.notifications:
            self._dbus_client.set_ups_notification_type(self.ups_data, NotificationType.LOW_BATTERY, False)
            self.notifications = self._dbus_client.get_all_ups_notifications(self.ups_data)


@Gtk.Template(resource_path='/org/ponderorg/UPSMonitor/ui/ups_settings_page.ui')
class UpsSettingsPage(Adw.NavigationPage):
    __gtype_name__ = 'UpsSettingsPage'

    window_title = Gtk.Template.Child()
    settings_group = Gtk.Template.Child()

    def __init__(self, **kwargs):
        self.ups_data = kwargs.get("ups_data", None)
        if self.ups_data != None:
            kwargs.pop("ups_data")
        super().__init__(**kwargs)
        self.window_title.set_title(self.ups_data.model)
        self._dbus_client = UPSMonitorClient()
        if "beeper.disable" in self.ups_data.cmds.keys() and "beeper.enable" in self.ups_data.cmds.keys():
            row = SettingSwitchRow()
            if self.ups_data.beeper_status == "enabled":
                row.switch_button.set_active(True)
            else:
                row.switch_button.set_active(False)
            row.switch_button.connect("notify::active", self.switch_beeper)
            row.set_title("Beeper enabled")
            self.settings_group.add(row)

    def switch_beeper(self, widget, args):
        if widget.get_active():
            self._dbus_client.run_command(self.ups_data.host_id, self.ups_data.key, "beeper.enable")
            self.ups_data.beeper_status = "enabled"
        else:
            self._dbus_client.run_command(self.ups_data.host_id, self.ups_data.key, "beeper.disable")
            self.ups_data.beeper_status = "disabled"

        #self.notifications = self._dbus_client.get_all_ups_notifications(self.ups_data)
        #if NotificationType.AUTO_SHUTDOWN in self.notifications:
        #@Gtk.Template.Callback()
        #def shutdown_low_battery_switch_selected(self, widget, args):
        #    if self.shutdown_low_battery_switch.get_active() and NotificationType.AUTO_SHUTDOWN not in self.notifications:
        #        self._dbus_client.set_ups_notification_type(self.ups_data, NotificationType.AUTO_SHUTDOWN, True)
        #        self.notifications = self._dbus_client.get_all_ups_notifications(self.ups_data)
        #    elif not self.shutdown_low_battery_switch.get_active() and NotificationType.AUTO_SHUTDOWN in self.notifications:
        #        self._dbus_client.set_ups_notification_type(self.ups_data, NotificationType.AUTO_SHUTDOWN, False)
        #        self.notifications = self._dbus_client.get_all_ups_notifications(self.ups_data)

@Gtk.Template(resource_path='/org/ponderorg/UPSMonitor/ui/home_page.ui')
class HomePage(Adw.NavigationPage):
    __gtype_name__ = 'HomePage'

    def __init__(self, **kwargs):
        super().__init__(**kwargs)


@Gtk.Template(resource_path='/org/ponderorg/UPSMonitor/ui/outlets_page.ui')
class OutletsPage(Adw.NavigationPage):
    __gtype_name__ = 'OutletsPage'

    outlets_page = Gtk.Template.Child()
    window_title = Gtk.Template.Child()

    def __init__(self, **kwargs):
        self.ups_data = kwargs.get("ups_data", None)
        if self.ups_data != None:
            kwargs.pop("ups_data")
        super().__init__(**kwargs)
        self.window_title.set_title(self.ups_data.model)
        for outlet in self.ups_data.outlet_collection.outlets:
            pg = Adw.PreferencesGroup()
            pg.set_title(outlet.desc)
            pg.set_visible(True)
            if outlet.switchable is not None:
                pg.add(CustomRow(title="Switchable", label=outlet.switchable))
            if outlet.status is not None:
                pg.add(CustomRow(title="Status", label=outlet.status))
            self.outlets_page.add(pg)


