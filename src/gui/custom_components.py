from gi.repository import Adw, Gtk
from .data_model import UPS
from .ups_monitor_daemon import UPSMonitorClient

class CustomExpanderRow(Adw.ExpanderRow):

    def __init__(self, **kwargs,):
        label_text = kwargs.get("label", None)
        if label_text != None:
            kwargs.pop("label")
        title = kwargs.get("title", None)
        if title != None:
            kwargs.pop("title")
        subtitle = kwargs.get("subtitle", None)
        if subtitle != None:
            kwargs.pop("subtitle")
        icon_name = kwargs.get("icon_name", None)
        if icon_name != None:
            kwargs.pop("icon_name")
        super().__init__(**kwargs)
        label = Gtk.Label()
        label.add_css_class("heading")
        label.set_label(label_text)
        if icon_name is not None:
            self.add_prefix(Gtk.Image(icon_name=icon_name))
        self.set_title(title)
        self.add_suffix(label)

class CustomRow(Adw.ActionRow):

    def __init__(self, **kwargs,):
        label_text = kwargs.get("label", None)
        if label_text != None:
            kwargs.pop("label")
        title = kwargs.get("title", None)
        if title != None:
            kwargs.pop("title")
        subtitle = kwargs.get("subtitle", None)
        if subtitle != None:
            kwargs.pop("subtitle")
        icon_name = kwargs.get("icon_name", None)
        if icon_name != None:
            kwargs.pop("icon_name")
        super().__init__(**kwargs)
        label = Gtk.Label()
        label.set_label(label_text)
        label.add_css_class("heading")
        if title is not None:
            self.set_title(title)
        if subtitle is not None:
            self.set_subtitle(subtitle)
        if icon_name is not None:
            self.add_prefix(Gtk.Image(icon_name=icon_name))
        self.add_suffix(label)

class CustomRealtimeRow(Adw.ActionRow):

    def __init__(self, **kwargs,):
        self.ups_data = kwargs.get("ups_data", None)
        if self.ups_data != None:
            kwargs.pop("ups_data")
        title = kwargs.get("title", None)
        if title != None:
            kwargs.pop("title")
        subtitle = kwargs.get("subtitle", None)
        if subtitle != None:
            kwargs.pop("subtitle")
        icon_name = kwargs.get("icon_name", None)
        if icon_name != None:
            kwargs.pop("icon_name")
        super().__init__(**kwargs)
        self.label = Gtk.Label()
        self.label.add_css_class("heading")
        if title is not None:
            self.set_title(title)
        if subtitle is not None:
            self.set_subtitle(subtitle)
        if icon_name is not None:
            self.add_prefix(Gtk.Image(icon_name=icon_name))
        self.add_suffix(self.label)
        self._dbus_client = UPSMonitorClient()
        self._dbus_signal_handler = self._dbus_client.connect_to_signal("ups_updated", self._update_self)
        self.connect("destroy", self.on_destroy)
        self._update_self()

    def _update_self(self):
        if self.ups_data != None and self.ups_data.host_id != None:
            ups_data = self._dbus_client.get_ups_by_name_and_host(self.ups_data.host_id, self.ups_data.key)
            if ups_data != None:
                self.ups_data = ups_data
        self.update_self()

    def update_self(self):
        pass

    def on_destroy(self, widget):
        self._dbus_signal_handler.remove()

class BeeperRow(CustomRealtimeRow):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.label.set_label(self.ups_data.beeper_status)

    def update_self(self):
        self.label.set_label(self.ups_data.beeper_status)

class OutletsRow(Adw.ActionRow):
    __gtype_name__ = 'OutletsRow'

    def __init__(self, **kwargs):
        label_text = kwargs.get("label", None)
        if label_text != None:
            kwargs.pop("label")
        title = kwargs.get("title", None)
        if title != None:
            kwargs.pop("title")
        subtitle = kwargs.get("subtitle", None)
        if subtitle != None:
            kwargs.pop("subtitle")
        icon_name = kwargs.get("icon_name", None)
        if icon_name != None:
            kwargs.pop("icon_name")
        super().__init__(**kwargs)
        if title is not None:
            self.set_title(title)
        if subtitle is not None:
            self.set_subtitle(subtitle)
        if icon_name is not None:
            self.add_prefix(Gtk.Image(icon_name=icon_name))
        box = Gtk.Box()
        if label_text is not None:
            button = Gtk.Button(label=label_text)
            button.add_css_class("circular")
            button.add_css_class("suggested-action")
            button.set_margin_top(10)
            button.set_margin_bottom(10)
            box.append(button)
        img_obj = Gtk.Image(icon_name="go-next-symbolic")
        img_obj.set_margin_start(20)
        box.append(img_obj)
        self.add_suffix(box)
        self.set_activatable(True)

@Gtk.Template(resource_path='/org/ponderorg/UPSMonitor/ui/setting_switch_row.ui')
class SettingSwitchRow(Adw.ActionRow):
    __gtype_name__ = 'SettingSwitchRow'

    switch_button = Gtk.Template.Child()
    info_button = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

@Gtk.Template(resource_path='/org/ponderorg/UPSMonitor/ui/load_row.ui')
class LoadRow(Adw.ActionRow):
    __gtype_name__ = 'LoadRow'

    load_level_bar = Gtk.Template.Child()
    load_image = Gtk.Template.Child()

    def __init__(self, **kwargs):
        self.ups_data = kwargs.get("ups_data", None)
        if self.ups_data != None:
            kwargs.pop("ups_data")
        super().__init__(**kwargs)
        self._dbus_client = UPSMonitorClient()
        self._dbus_signal_handler = self._dbus_client.connect_to_signal("ups_updated", self.update_self)
        self.connect("destroy", self.on_destroy)
        self.update_self()

    def update_self(self):
        if self.ups_data != None and self.ups_data.host_id != None:
            ups_data = self._dbus_client.get_ups_by_name_and_host(self.ups_data.host_id, self.ups_data.key)
            if ups_data != None:
                self.ups_data = ups_data
        load = int(self.ups_data.load)
        if load <= 33:
            image_name = "power-profile-power-saver-symbolic"
        elif load > 33 and load <= 66:
            image_name = "power-profile-balanced-symbolic"
        else:
            image_name = "power-profile-performance-symbolic"
        self.load_image.set_from_icon_name(image_name)
        self.load_level_bar.set_value(load)
        self.set_subtitle(str(load) + "%")

    def on_destroy(self, widget):
        self._dbus_signal_handler.remove()

@Gtk.Template(resource_path='/org/ponderorg/UPSMonitor/ui/battery_row.ui')
class BatteryRow(Adw.ExpanderRow):
    __gtype_name__ = 'BatteryRow'

    power_image = Gtk.Template.Child()
    level_bar = Gtk.Template.Child()

    def __init__(self, **kwargs):
        self.ups_data = kwargs.get("ups_data", None)
        if self.ups_data != None:
            kwargs.pop("ups_data")
        super().__init__(**kwargs)
        self._dbus_client = UPSMonitorClient()
        self._dbus_signal_handler = self._dbus_client.connect_to_signal("ups_updated", self.update_self)
        self.connect("destroy", self.on_destroy)
        if self.ups_data.battery.runtime is not None:
            self.add_row(self.create_row("Runtime", self.ups_data.battery.runtime))
        if self.ups_data.battery.type is not None:
            self.add_row(self.create_row("Type", self.ups_data.battery.type))
        self.update_self()

    def create_row(self, title:str, text:str, img:str=None):
        row = Adw.ActionRow()
        row.set_title(title)
        label = Gtk.Label()
        label.add_css_class("heading")
        label.set_label(text)
        if img is not None:
            img_obj = Gtk.Image(icon_name=img)
            row.add_prefix(img_obj)
        row.add_suffix(label)
        return row

    def update_self(self):
        if self.ups_data != None and self.ups_data.host_id != None:
            ups_data = self._dbus_client.get_ups_by_name_and_host(self.ups_data.host_id, self.ups_data.key)
            if ups_data != None:
                self.ups_data = ups_data
        charge = int(self.ups_data.battery.charge)
        image_name = self.get_image_name(self.ups_data)
        self.power_image.set_from_icon_name(image_name)
        self.set_subtitle(str(charge) + "%")
        self.level_bar.set_value(charge)

    def get_image_name(self, ups_data:dict):
        image_name = "battery-action-symbolic"
        charge = int(self.ups_data.battery.charge)
        if self.ups_data.status == "OB":
            if charge >= 90:
                image_name = "battery-full-symbolic"
            elif charge >= 80:
                image_name = "battery-level-90-symbolic"
            elif charge >= 70:
                image_name = "battery-level-70-symbolic"
            elif charge >= 60:
                image_name = "battery-level-60-symbolic"
            elif charge >= 50:
                image_name = "battery-level-50-symbolic"
            elif charge >= 40:
                image_name = "battery-level-40-symbolic"
            elif charge >= 30:
                image_name = "battery-level-30-symbolic"
            elif charge >= 20:
                image_name = "battery-level-20-symbolic"
            elif charge >= 10:
                image_name = "battery-leve-10-symbolic"
            else:
                image_name = "battery-level-0-symbolic"
        elif self.ups_data.status == "OL":
            if charge >= 90:
                image_name = "battery-full-charging-symbolic"
            elif charge >= 80:
                image_name = "battery-level-90-charging-symbolic"
            elif charge >= 70:
                image_name = "battery-level-70-charging-symbolic"
            elif charge >= 60:
                image_name = "battery-level-60-charging-symbolic"
            elif charge >= 50:
                image_name = "battery-level-50-charging-symbolic"
            elif charge >= 40:
                image_name = "battery-level-40-charging-symbolic"
            elif charge >= 30:
                image_name = "battery-level-30-charging-symbolic"
            elif charge >= 20:
                image_name = "battery-level-20-charging-symbolic"
            elif charge >= 10:
                image_name = "battery-level-10-charging-symbolic"
            else:
                image_name = "battery-level-0-charging-symbolic"
        return image_name

    def on_destroy(self, widget):
        self._dbus_signal_handler.remove()


@Gtk.Template(resource_path='/org/ponderorg/UPSMonitor/ui/ups_action_row.ui')
class UpsActionRow(Adw.ActionRow):
    __gtype_name__ = 'UpsActionRow'

    row_image = Gtk.Template.Child()

    def __init__(self, **kwargs):
        ups_data = kwargs.get("ups_data", None)
        if ups_data != None:
            kwargs.pop("ups_data")
        super().__init__(**kwargs)
        self._dbus_client = UPSMonitorClient()
        self._dbus_signal_handler = self._dbus_client.connect_to_signal("ups_updated", self.update_self)
        self.connect("destroy", self.on_destroy)
        self.update_self(ups_data)

    def update_self(self, ups_data:UPS=None):
        if ups_data != None:
            self.ups_data = ups_data
        elif ups_data == None  and self.ups_data.host_id != None:
            self.ups_data = self._dbus_client.get_ups_by_name_and_host(self.ups_data.host_id, self.ups_data.key)
        else:
            return
        self.set_title(self.ups_data.model)
        if self.ups_data.status == "N/A":
            image_name = "battery-action-symbolic"
        elif self.ups_data.status == "OB":
            self.set_subtitle("Offline")
            if int(self.ups_data.battery.charge) >= 90:
                image_name = "battery-full-symbolic"
            elif int(self.ups_data.battery.charge) >= 80:
                image_name = "battery-level-90-symbolic"
            elif int(self.ups_data.battery.charge) >= 70:
                image_name = "battery-level-70-symbolic"
            elif int(self.ups_data.battery.charge) >= 60:
                image_name = "battery-level-60-symbolic"
            elif int(self.ups_data.battery.charge) >= 50:
                image_name = "battery-level-50-symbolic"
            elif int(self.ups_data.battery.charge) >= 40:
                image_name = "battery-level-40-symbolic"
            elif int(self.ups_data.battery.charge) >= 30:
                image_name = "battery-level-30-symbolic"
            elif int(self.ups_data.battery.charge) >= 20:
                image_name = "battery-level-20-symbolic"
            elif int(self.ups_data.battery.charge) >= 10:
                image_name = "battery-leve-10-symbolic"
            else:
                image_name = "battery-level-0-symbolic"
        elif self.ups_data.status == "OL":
            self.set_subtitle("Online")
            if int(self.ups_data.battery.charge) >= 90:
                image_name = "battery-full-charging-symbolic"
            elif int(self.ups_data.battery.charge) >= 80:
                image_name = "battery-level-90-charging-symbolic"
            elif int(self.ups_data.battery.charge) >= 70:
                image_name = "battery-level-70-charging-symbolic"
            elif int(self.ups_data.battery.charge) >= 60:
                image_name = "battery-level-60-charging-symbolic"
            elif int(self.ups_data.battery.charge) >= 50:
                image_name = "battery-level-50-charging-symbolic"
            elif int(self.ups_data.battery.charge) >= 40:
                image_name = "battery-level-40-charging-symbolic"
            elif int(self.ups_data.battery.charge) >= 30:
                image_name = "battery-level-30-charging-symbolic"
            elif int(self.ups_data.battery.charge) >= 20:
                image_name = "battery-level-20-charging-symbolic"
            elif int(self.ups_data.battery.charge) >= 10:
                image_name = "battery-level-10-charging-symbolic"
            else:
                image_name = "battery-level-0-charging-symbolic"
        self.row_image.set_from_icon_name(image_name)

    def on_destroy(self, widget):
        self._dbus_signal_handler.remove()
