import enum
from typing import Optional
from gi.repository import GObject

class Host(GObject.Object):
    __gtype_name__ = 'Host'

    def __init__(self, ip_address:str=None, port:int=None, profile_name:str=None, host_id:int=None, username:str=None, password:str=None, host_dict:dict=None):
        super().__init__()
        if ip_address and port :
            self.host_id = host_id
            self.profile_name = profile_name
            self.ip_address = ip_address
            self.port = port
            self.username = username
            self.password = password

        elif host_dict:
            if 'host_id' in host_dict.keys():
                self.host_id = host_dict['host_id']
            else :
                self.host_id = None
            if 'profile_name' in host_dict.keys():
                self.profile_name = host_dict['profile_name']
            else :
                self.profile_name = None
            if 'ip_address' in host_dict.keys():
                self.ip_address = host_dict['ip_address']
            else :
                self.ip_address = None
            if 'port' in host_dict.keys():
                self.port = host_dict['port']
            else :
                self.port = None
            if 'username' in host_dict.keys():
                self.username = host_dict['username']
            else :
                self.username = None
            if 'password' in host_dict.keys():
                self.password = host_dict['password']
            else :
                self.password = None
        else:
            raise Exception('Missing minimum host parameters in the constructor')

class Battery(GObject.GObject):
    __gtype_name__ = 'Battery'

    def __init__(self, data: dict):
        super().__init__()
        mapping = {
            'battery.charge': 'charge',
            'battery.charge.approx': 'charge_approx',
            'battery.charge.low': 'charge_low',
            'battery.charge.restart': 'charge_restart',
            'battery.charge.warning': 'charge_warning',
            'battery.charger.status': 'charger_status',
            'battery.voltage': 'voltage',
            'battery.voltage.cell.max': 'voltage_cell_max',
            'battery.voltage.cell.min': 'voltage_cell_min',
            'battery.voltage.nominal': 'voltage_nominal',
            'battery.voltage.low': 'voltage_low',
            'battery.voltage.high': 'voltage_high',
            'battery.capacity': 'capacity',
            'battery.capacity.nominal': 'capacity_nominal',
            'battery.current': 'current',
            'battery.current.total': 'current_total',
            'battery.status': 'status',
            'battery.temperature': 'temperature',
            'battery.temperature.cell.max': 'temperature_cell_max',
            'battery.temperature.cell.min': 'temperature_cell_min',
            'battery.runtime': 'runtime',
            'battery.runtime.low': 'runtime_low',
            'battery.runtime.restart': 'runtime_restart',
            'battery.alarm.threshold': 'alarm_threshold',
            'battery.date': 'date',
            'battery.date.maintenance': 'date_maintenance',
            'battery.mfr.date': 'mfr_date',
            'battery.packs': 'packs',
            'battery.packs.bad': 'packs_bad',
            'battery.packs.external': 'packs_external',
            'battery.type': 'type',
            'battery.protection': 'protection',
            'battery.energysave': 'energysave',
            'battery.energysave.load': 'energysave_load',
            'battery.energysave.delay': 'energysave_delay',
            'battery.energysave.realpower': 'energysave_realpower'
        }
        for key, attr in mapping.items():
            if key in data:
                setattr(self, attr, data[key])
            else:
                setattr(self, attr, None)

class Device(GObject.GObject):
    __gtype_name__ = 'Device'

    def __init__(self, data: dict):
        super().__init__()
        mapping = {
            'device.model': 'model',
            'device.mfr': 'mfr',
            'device.serial': 'serial',
            'device.type': '_type',
            'device.description': 'description',
            'device.contact': 'contact',
            'device.location': 'location',
            'device.part': 'part',
            'device.macaddr': 'macaddr',
            'device.uptime': 'uptime',
            'device.count': 'count'
        }
        for key, attr in mapping.items():
            if key in data:
                setattr(self, attr, data[key])
            else:
                setattr(self, attr, None)

class Driver(GObject.GObject):
    __gtype_name__ = 'Driver'

    def __init__(self, data: dict):
        super().__init__()
        mapping = {
            'driver.name': 'name',
            'driver.version': 'version',
            'driver.version.internal': 'version_internal',
            'driver.version.data': 'version_data',
            'driver.version.usb': 'version_usb',
            'driver.parameter.xxx': 'parameter_xxx',
            'driver.flag.xxx': 'flag_xxx',
            'driver.state': 'state',
            "driver.debug": "debug",
            "driver.flag.allow_killpower": "flag_allow_killpower",
            "driver.parameter.bus": "bus",
            "driver.parameter.pollfreq": "pollfreq",
            "driver.parameter.pollinterval": "pollinterval",
            "driver.parameter.port": "port",
            "driver.parameter.product": "product",
            "driver.parameter.productid": "productid",
            "driver.parameter.serial": "serial",
            "driver.parameter.synchronous": "synchronous",
            "driver.parameter.vendor": "vendor",
            "driver.parameter.vendorid": "vendorid",
        }
        for key, attr in mapping.items():
            if key in data:
                setattr(self, attr, data[key])
            else:
                setattr(self, attr, None)

class Output(GObject.GObject):
    __gtype_name__ = 'Output'

    def __init__(self, data: dict):
        super().__init__()
        mapping = {
            'output.voltage': 'voltage',
            'output.voltage.nominal': 'voltage_nominal',
            'output.frequency': 'frequency',
            'output.frequency.nominal': 'frequency_nominal',
            'output.current': 'current',
            'output.current.nominal': 'current_nominal'
        }
        for key, attr in mapping.items():
            if key in data:
                setattr(self, attr, data[key])
            else:
                setattr(self, attr, None)

class Input(GObject.GObject):
    __gtype_name__ = 'Input'

    def __init__(self, data: dict):
        super().__init__()

        # Dizionario di mapping tra le chiavi del dizionario di input e gli attributi della classe
        mapping = {
            'input.voltage': 'voltage',
            'input.voltage.maximum': 'voltage_maximum',
            'input.voltage.minimum': 'voltage_minimum',
            'input.voltage.status': 'voltage_status',
            'input.voltage.low.warning': 'voltage_low_warning',
            'input.voltage.low.critical': 'voltage_low_critical',
            'input.voltage.high.warning': 'voltage_high_warning',
            'input.voltage.high.critical': 'voltage_high_critical',
            'input.voltage.nominal': 'voltage_nominal',
            'input.voltage.extended': 'voltage_extended',
            'input.transfer.delay': 'transfer_delay',
            'input.transfer.reason': 'transfer_reason',
            'input.transfer.low': 'transfer_low',
            'input.transfer.high': 'transfer_high',
            'input.transfer.low.min': 'transfer_low_min',
            'input.transfer.low.max': 'transfer_low_max',
            'input.transfer.high.min': 'transfer_high_min',
            'input.transfer.high.max': 'transfer_high_max',
            'input.sensitivity': 'sensitivity',
            'input.quality': 'quality',
            'input.current': 'current',
            'input.current.nominal': 'current_nominal',
            'input.current.status': 'current_status',
            'input.current.low.warning': 'current_low_warning',
            'input.current.low.critical': 'current_low_critical',
            'input.current.high.warning': 'current_high_warning',
            'input.current.high.critical': 'current_high_critical',
            'input.feed.color': 'feed_color',
            'input.feed.desc': 'feed_desc',
            'input.frequency': 'frequency',
            'input.frequency.nominal': 'frequency_nominal',
            'input.frequency.status': 'frequency_status',
            'input.frequency.low': 'frequency_low',
            'input.frequency.high': 'frequency_high',
            'input.frequency.extended': 'frequency_extended',
            'input.transfer.boost.low': 'transfer_boost_low',
            'input.transfer.boost.high': 'transfer_boost_high',
            'input.transfer.trim.low': 'transfer_trim_low',
            'input.transfer.trim.high': 'transfer_trim_high',
            'input.transfer.eco.low': 'transfer_eco_low',
            'input.transfer.bypass.low': 'transfer_bypass_low',
            'input.transfer.eco.high': 'transfer_eco_high',
            'input.transfer.bypass.high': 'transfer_bypass_high',
            'input.transfer.frequency.bypass.range': 'transfer_frequency_bypass_range',
            'input.transfer.frequency.eco.range': 'transfer_frequency_eco_range',
            'input.transfer.hysteresis': 'transfer_hysteresis',
            'input.load': 'load',
            'input.realpower': 'realpower',
            'input.realpower.nominal': 'realpower_nominal',
            'input.power': 'power',
            'input.source': 'source',
            'input.source.preferred': 'source_preferred',
            'input.phase.shift': 'phase_shift'
        }

        # Imposta gli attributi della classe basandosi sui dati forniti
        for key, attr in mapping.items():
            if key in data:
                setattr(self, attr, data[key])
            else:
                setattr(self, attr, None)


class Outlet(GObject.GObject):
    __gtype_name__ = 'Outlet'

    def __init__(self, data: dict):
        super().__init__()
        mapping = {
            'outlet.id': 'id',
            'outlet.name': 'name',
            'outlet.desc': 'desc',
            'outlet.groupid': 'groupid',
            'outlet.switch': 'switch',
            'outlet.status': 'status',
            'outlet.alarm': 'alarm',
            'outlet.switchable': 'switchable',
            'outlet.autoswitch.charge.low': 'autoswitch_charge_low',
            'outlet.battery.charge.low': 'battery_charge_low',
            'outlet.delay.shutdown': 'delay_shutdown',
            'outlet.delay.start': 'delay_start',
            'outlet.timer.shutdown': 'timer_shutdown',
            'outlet.timer.start': 'timer_start',
            'outlet.current': 'current',
            'outlet.current.maximum': 'current_maximum',
            'outlet.current.status': 'current_status',
            'outlet.current.low.warning': 'current_low_warning',
            'outlet.current.low.critical': 'current_low_critical',
            'outlet.current.high.warning': 'current_high_warning',
            'outlet.current.high.critical': 'current_high_critical',
            'outlet.realpower': 'realpower',
            'outlet.voltage': 'voltage',
            'outlet.voltage.status': 'voltage_status',
            'outlet.voltage.low.warning': 'voltage_low_warning',
            'outlet.voltage.low.critical': 'voltage_low_critical',
            'outlet.voltage.high.warning': 'voltage_high_warning',
            'outlet.voltage.high.critical': 'voltage_high_critical',
            'outlet.powerfactor': 'powerfactor',
            'outlet.crestfactor': 'crestfactor',
            'outlet.power': 'power',
            'outlet.type': 'type'
        }
        for key, attr in mapping.items():
            if key in data:
                setattr(self, attr, data[key])
            else:
                setattr(self, attr, None)

class OutletCollection(GObject.GObject):
    __gtype_name__ = 'OutletCollection'

    def __init__(self, data: dict):
        super().__init__()
        self.count = 0
        self.switchable = data.get('outlet.switchable', False)
        self.outlets = []
        noid_outlet = Outlet(data)
        if noid_outlet.id is not None:
            self.outlets.append(noid_outlet)
            self.count += 1
        for i in range(1, self.count + 1):
            outlet_data = {key.replace(f'outlet.{i}.', 'outlet.'): value
                           for key, value in data.items()
                           if key.startswith(f'outlet.{i}.')}
            self.outlets.append(Outlet(outlet_data))
            self.count += 1

class AmbientSensor(GObject.GObject):
    __gtype_name__ = 'AmbientSensor'

    def __init__(self, data: dict):
        super().__init__()
        mapping = {
            'ambient.name': 'name',
            'ambient.id': 'id',
            'ambient.address': 'address',
            'ambient.parent.serial': 'parent_serial',
            'ambient.mfr': 'mfr',
            'ambient.model': 'model',
            'ambient.firmware': 'firmware',
            'ambient.present': 'present',
            'ambient.temperature': 'temperature',
            'ambient.temperature.alarm': 'temperature_alarm',
            'ambient.temperature.status': 'temperature_status',
            'ambient.temperature.high': 'temperature_high',
            'ambient.temperature.high.warning': 'temperature_high_warning',
            'ambient.temperature.high.critical': 'temperature_high_critical',
            'ambient.temperature.low': 'temperature_low',
            'ambient.temperature.low.warning': 'temperature_low_warning',
            'ambient.temperature.low.critical': 'temperature_low_critical',
            'ambient.temperature.maximum': 'temperature_maximum',
            'ambient.temperature.minimum': 'temperature_minimum',
            'ambient.humidity': 'humidity',
            'ambient.humidity.alarm': 'humidity_alarm',
            'ambient.humidity.status': 'humidity_status',
            'ambient.humidity.high': 'humidity_high',
            'ambient.humidity.high.warning': 'humidity_high_warning',
            'ambient.humidity.high.critical': 'humidity_high_critical',
            'ambient.humidity.low': 'humidity_low',
            'ambient.humidity.low.warning': 'humidity_low_warning',
            'ambient.humidity.low.critical': 'humidity_low_critical',
            'ambient.humidity.maximum': 'humidity_maximum',
            'ambient.humidity.minimum': 'humidity_minimum',
            'ambient.contacts.x.status': 'contacts_status',
            'ambient.contacts.x.config': 'contacts_config',
            'ambient.contacts.x.name': 'contacts_name'
        }
        for key, attr in mapping.items():
            if key in data:
                setattr(self, attr, data[key])
            else:
                setattr(self, attr, None)

class AmbientSensorCollection(GObject.GObject):
    __gtype_name__ = 'AmbientSensorCollection'

    def __init__(self, data: dict):
        super().__init__()

        # Numero di sensori ambientali
        self.sensor_count = data.get('ambient.count', 0)

        # Lista di oggetti AmbientSensor
        self.sensors = []

        # Creazione degli oggetti AmbientSensor basati sui dati
        for i in range(1, self.count + 1):
            sensor_data = {key.replace(f'ambient.{i}.', 'ambient.'): value
                           for key, value in data.items()
                           if key.startswith(f'ambient.{i}.')}
            self.sensors.append(AmbientSensor(sensor_data))

class Power(GObject.GObject):
    __gtype_name__ = 'Power'

    def __init__(self, data: dict):
        super().__init__()

        # Dizionario di mapping tra le chiavi del dizionario di input e gli attributi della classe
        mapping = {
            'power': 'value',
            'power.maximum': 'maximum',
            'power.minimum': 'minimum',
            'power.percent': 'percent',
            'power.maximum.percent': 'maximum_percent',
            'power.minimum.percent': 'minimum_percent'
        }

        # Imposta gli attributi della classe basandosi sui dati forniti
        for key, attr in mapping.items():
            if key in data:
                setattr(self, attr, data[key])
            else:
                setattr(self, attr, None)

class Voltage(GObject.GObject):
    __gtype_name__ = 'Voltage'

    def __init__(self, data: dict):
        super().__init__()

        # Dizionario di mapping tra le chiavi del dizionario di input e gli attributi della classe
        mapping = {
            'voltage': 'value',
            'voltage.nominal': 'nominal',
            'voltage.maximum': 'maximum',
            'voltage.minimum': 'minimum',
            'voltage.status': 'status',
            'voltage.low.warning': 'low_warning',
            'voltage.low.critical': 'low_critical',
            'voltage.high.warning': 'high_warning',
            'voltage.high.critical': 'high_critical'
        }

        # Imposta gli attributi della classe basandosi sui dati forniti
        for key, attr in mapping.items():
            if key in data:
                setattr(self, attr, data[key])
            else:
                setattr(self, attr, None)

class Current(GObject.GObject):
    __gtype_name__ = 'Current'

    def __init__(self, data: dict):
        super().__init__()

        # Dizionario di mapping tra le chiavi del dizionario di input e gli attributi della classe
        mapping = {
            'current': 'value',
            'current.maximum': 'maximum',
            'current.minimum': 'minimum',
            'current.status': 'status',
            'current.low.warning': 'low_warning',
            'current.low.critical': 'low_critical',
            'current.high.warning': 'high_warning',
            'current.high.critical': 'high_critical',
            'current.peak': 'peak'
        }

        # Imposta gli attributi della classe basandosi sui dati forniti
        for key, attr in mapping.items():
            if key in data:
                setattr(self, attr, data[key])
            else:
                setattr(self, attr, None)

class NotificationType(enum.Enum):

    IS_OFFLINE = 1, 'IS OFFLINE'
    LOW_BATTERY = 2, 'LOW BATTERY'
    AUTO_SHUTDOWN = 3, 'AUTO BATTERY'

    def __new__(cls, value, name):
        member = object.__new__(cls)
        member._value_ = value
        member.fullname = name
        return member

    def __int__(self):
        return self.value

class UPS(GObject.GObject):
    __gtype_name__ = 'UPS'

    def __init__(self, key:str,
                 name:str,
                 host_id:int,
                 data: dict,
                 device:Device = None,
                 battery:Battery = None,
                 driver:Driver = None,
                 _input:Input = None,
                 _output:Output = None,
                 current:Current = None,
                 voltage:Voltage = None,
                 power:Power = None,
                 ambient_sensor_collection:AmbientSensorCollection = None,
                 outlet_collection:OutletCollection = None):
        super().__init__()
        self.host_id = host_id
        self.key = key
        self.name = name
        self.device = device
        self.battery = battery
        self.driver = driver
        self._input = _input
        self._output = _output
        self.current = current
        self.voltage = voltage
        self.power = power
        self.ambient_sensor_collection = ambient_sensor_collection
        self.outlet_collection = outlet_collection
        self.notification_types = {}
        if 'commands' in data.keys():
            self.cmds = data['commands']
        mapping = {
            'ups.status': 'status',
            'ups.alarm': 'alarm',
            'ups.time': 'time',
            'ups.date': 'date',
            'ups.model': 'model',
            'ups.mfr': 'mfr',
            'ups.mfr.date': 'mfr_date',
            'ups.serial': 'serial',
            'ups.vendorid': 'vendorid',
            'ups.productid': 'productid',
            'ups.firmware': 'firmware',
            'ups.firmware.aux': 'firmware_aux',
            'ups.temperature': 'temperature',
            'ups.load.high': 'load_high',
            'ups.id': '_id',
            'ups.delay.start': 'delay_start',
            'ups.delay.reboot': 'delay_reboot',
            'ups.delay.shutdown': 'delay_shutdown',
            'ups.timer.start': 'timer_start',
            'ups.timer.reboot': 'timer_reboot',
            'ups.timer.shutdown': 'timer_shutdown',
            'ups.test.interval': 'test_interval',
            'ups.test.result': 'test_result',
            'ups.test.date': 'test_date',
            'ups.display.language': 'display_language',
            'ups.contacts': 'contacts',
            'ups.efficiency': 'efficiency',
            'ups.power': 'power',
            'ups.power.nominal': 'power_nominal',
            'ups.realpower': 'realpower',
            'ups.realpower.nominal': 'realpower_nominal',
            'ups.beeper.status': 'beeper_status',
            'enabled': 'enabled',
            'ups.type': '_type',
            'ups.watchdog.status': 'watchdog_status',
            'ups.start.auto': 'start_auto',
            'ups.start.battery': 'start_battery',
            'ups.start.reboot': 'start_reboot',
            'ups.shutdown': 'shutdown',
            'alarm' : 'alarm',
            'realpower' : 'realpower',
            'powerfactor' : 'powerfactor',
            'crestfactor' : 'crestfactor',
            'ups.load' : 'load',
            'frequency' : 'frequency',
            'frequency.nominal' : 'frequency_nominal'
        }
        for key, attr in mapping.items():
            if key in data:
                setattr(self, attr, data[key])
            else:
                setattr(self, attr, None)

