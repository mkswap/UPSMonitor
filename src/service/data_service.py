import sqlite3, os
import asyncio
import threading
from typing import Optional
from gi.repository import GObject
from .data_model import UPS, Host
from typing import Optional

class NUTClientError(Exception):
    def __init__(self, message):
        super().__init__(message)

class NUTClient:
    def __init__(self, host: str = '127.0.0.1', port: int = 3493, username: Optional[str] = None, password: Optional[str] = None):
        self._host = host
        self._port = port
        self._username = username
        self._password = password
        self.reader = None
        self.writer = None

        # Lock per sincronizzare l'accesso alle operazioni di rete
        self._lock = asyncio.Lock()

        # Variabile per memorizzare errori dal thread principale
        self._error = None

        # Creazione di un nuovo event loop e avvio del thread
        self.loop = asyncio.new_event_loop()
        self.thread = threading.Thread(target=self._start_loop, daemon=True)
        self.thread.start()

    def _start_loop(self):
        """Funzione per avviare l'event loop nel thread separato"""
        asyncio.set_event_loop(self.loop)
        try:
            self.loop.run_forever()
        except Exception as e:
            self._error = e
        finally:
            self.loop.stop()

    def _run_in_loop(self, coro):
        """Esegue la coroutine all'interno del loop del thread separato e restituisce il risultato"""
        future = asyncio.run_coroutine_threadsafe(coro, self.loop)
        result = future.result()
        if self._error:
            raise self._error
        return result

    async def _connect(self):
        async with self._lock:
            if not self.reader or not self.writer:
                self.reader, self.writer = await asyncio.open_connection(self._host, self._port)
                if self._username:
                    self.writer.write(f"USERNAME {self._username}\r\n".encode())
                    await self.writer.drain()
                    data = await self.reader.read(100)
                    _result = data.decode()
                    if "OK" not in _result:
                        raise NUTClientError(f"LOGIN ERROR (USERNAME): {_result}")

                if self._password:
                    self.writer.write(f"PASSWORD {self._password}\r\n".encode())
                    await self.writer.drain()
                    data = await self.reader.read(100)
                    _result = data.decode()
                    if "OK" not in _result:
                        raise NUTClientError(f"LOGIN ERROR (PASSWORD): {_result}")

    async def _disconnect(self):
        async with self._lock:
            if self.writer:
                self.writer.close()
                await self.writer.wait_closed()
                self.writer = None
                self.reader = None

    async def _send_command(self, command):
        async with self._lock:
            if self.writer:
                self.writer.write(f"{command}\r\n".encode())
                await self.writer.drain()
                response = []
                data = await self.reader.read(300)
                decoded_data = data.decode()
                if "ERR" in decoded_data:
                    raise NUTClientError(decoded_data.replace("ERR ", ""))
                response.append(decoded_data)
                if "BEGIN" in decoded_data:
                    while True:
                        data = await self.reader.read(300)
                        if not data:
                            break
                        decoded_data = data.decode()
                        response.append(decoded_data)
                        if "END" in decoded_data:
                            break
                return ''.join(response)
            else:
                raise NUTClientError("Not connected")

    # Metodi asincroni per ottenere dati

    async def _get_all_ups_async(self):
        await self._connect()
        response = await self._send_command("LIST UPS")
        await self._disconnect()
        ups_names = {}
        for line in response.splitlines():
            if line.startswith("UPS"):
                parts = line.split(" ", 2)
                if len(parts) >= 2:
                    ups_names[parts[1]] = parts[2]
        return ups_names

    async def _get_ups_var_async(self, ups_id: str):
        await self._connect()
        response = await self._send_command(f"LIST VAR {ups_id}")
        await self._disconnect()
        ups_vars = {}
        for line in response.splitlines():
            if line.startswith("VAR"):
                parts = line.split(" ", 3)
                if len(parts) >= 2:
                    ups_vars[parts[2]] = parts[3].strip('"')
        return ups_vars

    async def _get_ups_rw_var_async(self, ups_id: str):
        await self._connect()
        response = await self._send_command(f"LIST RW {ups_id}")
        await self._disconnect()
        ups_vars = {}
        for line in response.splitlines():
            if line.startswith("RW"):
                parts = line.split(" ", 3)
                if len(parts) >= 2:
                    ups_vars[parts[2]] = parts[3].strip('"')
        return ups_vars

    async def _set_ups_rw_var_async(self, ups_id: str, var: str, value: str):
        await self._connect()
        await self._send_command(f"SET VAR {ups_id} {var} {value}")
        await self._disconnect()
        return True

    async def _get_ups_cmd_async(self, ups_id: str):
        await self._connect()
        response = await self._send_command(f"LIST CMD {ups_id}")
        ups_cmds = {}
        for line in response.splitlines():
            if line.startswith("CMD"):
                parts = line.split(" ", 3)
                if len(parts) >= 2:
                    cmd_desc = await self._send_command(f"GET CMDDESC {ups_id} {parts[2]}")
                    desc = cmd_desc.split(" ", 3)
                    if len(desc) >= 2:
                        ups_cmds[parts[2]] = desc[3].replace('\n', '').strip('"')
                    else:
                        ups_cmds[parts[2]] = ''
        await self._disconnect()
        return ups_cmds

    async def _get_ver(self):
        await self._connect()
        result = await self._send_command("VER")
        await self._disconnect()
        return result.rstrip()

    async def _get_prot_ver(self):
        await self._connect()
        result = await self._send_command("PROTVER")
        await self._disconnect()
        return result.rstrip()

    async def _get_help(self):
        await self._connect()
        result = await self._send_command("HELP")
        await self._disconnect()
        return result.rstrip()

    async def _instcmd(self, ups_id: str, cmd: str):
        await self._connect()
        result = await self._send_command(f"INSTCMD {ups_id} {cmd}")
        await self._disconnect()
        return True

    # Metodi pubblici chiamabili dall'esterno

    def get_all_ups(self):
        """Restituisce l'elenco di tutti gli UPS (bloccante)"""
        return self._run_in_loop(self._get_all_ups_async())

    def get_ups_vars(self, ups_id: str):
        """Restituisce le variabili di un UPS specifico (bloccante)"""
        ups_vars = self._run_in_loop(self._get_ups_var_async(ups_id))
        return ups_vars
    def get_ups_rw_vars(self, ups_id: str):
        """Restituisce le variabili di un UPS specifico (bloccante)"""
        return self._run_in_loop(self._get_ups_rw_var_async(ups_id))

    def set_ups_rw_vars(self, ups_id: str, var: str, value: str):
        """Imposta una variabile RW di un UPS specifico (bloccante)"""
        return self._run_in_loop(self._set_ups_rw_var_async(ups_id, var, value))

    def get_ups_cmds(self, ups_id: str):
        """Restituisce i comandi disponibili per un UPS specifico (bloccante)"""
        return self._run_in_loop(self._get_ups_cmd_async(ups_id))

    def ver(self):
        """Restituisce la versione del protocollo (bloccante)"""
        return self._run_in_loop(self._get_ver())

    def prot_ver(self):
        """Restituisce la versione del protocollo di protezione (bloccante)"""
        return self._run_in_loop(self._get_prot_ver())

    def help(self):
        """Restituisce l'help del comando (bloccante)"""
        return self._run_in_loop(self._get_help())

    def instcmd(self, ups_id: str, cmd: str):
        """Esegue un comando istantaneo su un UPS specifico (bloccante)"""
        return self._run_in_loop(self._instcmd(ups_id, cmd))

    def stop(self):
        """Ferma l'event loop e chiude il thread"""
        self.loop.call_soon_threadsafe(self.loop.stop)
        self.thread.join()
        if self._error:
            raise self._error

class UPServices(GObject.Object):
    ___gtype_name__ = 'UPServices'

    def __init__(self, host:dict):
        super().__init__()
        self.host = host
        if host['username'] != None and host['password'] != None:
            self.client = NUTClient(host=host['ip_address'], username=host['username'], password=host['password'], port=host['port'])
        else:
            self.client = NUTClient(host=host['ip_address'], port=host['port'])

    def get_all_hosts_ups(self):
        ups_list = []
        ups_dict = self.client.get_all_ups()
        for k1, v1 in ups_dict.items():
            vars_dict = self.client.get_ups_vars(k1)
            rw_vars_dict = self.client.get_ups_rw_vars(k1)
            cmds_dict = self.client.get_ups_cmds(k1)
            additional_vars = { "name" : k1 , "name.pretty" : v1 }
            if isinstance(self.host['host_id'], int):
                additional_vars['host_id'] = self.host['host_id']
            else:
                additional_vars['host_id'] = 'None'
            if len(cmds_dict.keys()) > 0:
                additional_vars['commands'] = cmds_dict
            if len(rw_vars_dict.keys()) > 0:
                additional_vars['writable'] = [*rw_vars_dict.keys()]
            vars_dict.update(additional_vars)
            ups_list.append(vars_dict)
        return ups_list

    def run_command(self, ups_name:str, str_command:str):
        self.client.instcmd(ups_name, str_command)

class HostServices(GObject.Object):
    __gtype_name__ = 'HostServices'

    def __init__(self):
        super().__init__()
        self.conn = sqlite3.connect('.var/app/org.ponderorg.UPSMonitor/data/ups_monitor.db')
        cursor = self.conn.cursor()
        cursor.execute('''CREATE TABLE IF NOT EXISTS hosts
                             (id             INTEGER    PRIMARY KEY     AUTOINCREMENT,
                              profile_name   CHAR(50),
                              ip_address     TEXT       NOT NULL,
                              port           INTEGER    NOT NULL,
                              username       CHAR(50),
                              password       CHAR(50));''')
        cursor.execute('''CREATE TABLE IF NOT EXISTS notifications
                             (id            INTEGER     PRIMARY KEY     AUTOINCREMENT,
                              name          CHAR(50),
                              host_id       INTEGER     NOT NULL,
                              type          INTEGER     NOT NULL);''')
        self.conn.commit()

    def get_all_hosts(self) -> []:
        cursor = self.conn.cursor()
        result = cursor.execute("SELECT id, profile_name, ip_address, port, username, password FROM hosts")
        host_list = []
        for row in result:
            host_list.append({'host_id':row[0], 'profile_name':row[1], 'ip_address':row[2], 'port':row[3], 'username':row[4], 'password':row[5]})
        return host_list

    def get_host(self, host_id:int) -> []:
        cursor = self.conn.cursor()
        result = cursor.execute("SELECT id, profile_name, ip_address, port, username, password FROM hosts WHERE id=?", (host_id,))
        for row in result:
           return {'host_id':row[0], 'profile_name':row[1], 'ip_address':row[2], 'port':row[3], 'username':row[4], 'password':row[5]}
        return None

    def get_host_by_name(self, host_name:str) -> []:
        cursor = self.conn.cursor()
        result = cursor.execute("SELECT id, profile_name, ip_address, port, username, password FROM hosts WHERE profile_name=?", (host_name,))
        for row in result:
            return {'host_id':row[0], 'profile_name':row[1], 'ip_address':row[2], 'port':row[3], 'username':row[4], 'password':row[5]}
        return None

    def set_ups_notification_type(self, host_id:int, ups_name:str, notification_type:int, active:bool = True) -> []:
        cursor = self.conn.cursor()
        notification_types = {}
        cursor.execute("SELECT * FROM hosts WHERE id=?", (host_id,))
        if cursor.fetchone() is not None:
            cursor.execute("SELECT * FROM notifications WHERE name=? AND host_id=? AND type=?", (ups_name, host_id, int(notification_type)))
            if active and cursor.fetchone() is None:
                result = cursor.execute("INSERT INTO notifications (name, host_id, type) VALUES (?,?,?)", (ups_name, host_id, int(notification_type)))
            elif not active :
                result = cursor.execute("DELETE FROM notifications WHERE name=? AND host_id=? AND type=?", (ups_name, host_id, int(notification_type)))
            self.conn.commit()
        else:
            pass

    def get_all_ups_notifications(self, host_id:int, ups_name:str) -> []:
        cursor = self.conn.cursor()
        notification_types = []
        result = cursor.execute("SELECT type FROM notifications WHERE name=? AND host_id=?", (ups_name, host_id,))
        for notification_type in result:
            notification_types.append(notification_type[0])
        return notification_types

    def save_host(self, host:dict):
        cursor = self.conn.cursor()
        cursor.execute("SELECT * FROM hosts WHERE profile_name=?", (host['profile_name'],))
        if cursor.fetchone() is not None:
            raise HostNameAlreadyExist
        cursor.execute("SELECT * FROM hosts WHERE ip_address=?", (host['ip_address'],))
        if cursor.fetchone() is not None:
            raise HostAddressAlreadyExist
        if host['username'] != None or host['username'] != None:
            cursor.execute("INSERT INTO hosts (profile_name, ip_address, port,username, password) VALUES (?,?,?,?,?)", (host['profile_name'], host['ip_address'], host['port'], host['username'], host['password']))
        else:
            cursor.execute("INSERT INTO hosts (profile_name, ip_address, port) VALUES (?,?,?)", (host['profile_name'], host['ip_address'], host['port']))
        self.conn.commit()

    def update_host(self, host:dict):
        cursor = self.conn.cursor()
        cursor.execute("UPDATE hosts SET ip_address=?, port=?, profile_name=?, username=?, password=? WHERE id=?", (host['ip_address'], host['port'], host['profile_name'], host['username'], host['password'], host['host_id']))
        self.conn.commit()

    def delete_host(self, host_id:int):
        self.conn.execute("DELETE FROM hosts WHERE id=?", (host_id,))
        self.conn.execute("DELETE FROM notifications WHERE host_id=?", (host_id,))
        self.conn.commit()
